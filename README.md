# Contact Manager

# Comments
Apologies, I am currently working full-time so I have only had time
to carry out the minimum functionality in this tech test, I hope that
is sufficient

I chose to use Redux rather than GraphQL local cache for state management
because I understand you are currently using Redux at Drugdev?

Given further time I would have liked to have enhanced the styles,
added some Unit test scripts, better error handling, handling edge cases etc

Paul Fallon 19-9-19
07966 409671

## Features to implement

- List Contacts
- View Contact
- Add Contact
- Delete Contact
- Edit Contact
- Clean routing i.e '/contact/:id'
- Use material-ui for components

### Bonuses

- Use [xstate](https://xstate.js.org/docs) to manage app state.
- Connect to the graphql endpoint http://localhost:3001 by using create-react-app proxy feature
- Use the graphql endpoint to get/create/update/delete

