import React, { Component } from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Menu from "./components/Menu/Menu";
import ListContact from "./components/ListContact/ListContact";
import CreateContact from "./components/CreateContact/CreateContact";
import ViewContact from "./components/ViewContact/ViewContact";
import EditContact from "./components/EditContact/EditContact";

// Apologies, I am currently working full-time so I have only had time
// to carry out the minimum functionality in the tech test, I hope that
// is sufficient

// I chose to use Redux rather than GraphQL local cache for state management
// because I understand you are currently using Redux at drugdev?

// Given further time I would have liked to have enhanced the styles,
// added some Unit test scripts, better error handling, handling edge cases etc

// Paul Fallon 19-9-19
// 07966 409671

const App = () => (
  <Router>
    <div className="App">
      <Menu />
      <Route path="/contacts" exact component={ListContact} />
      <Route path="/contacts/new" exact component={CreateContact} />
      <Route path="/contact/:id" exact component={ViewContact} />
      <Route path="/contact/:id/edit" exact component={EditContact} />
    </div>
  </Router>
);

export default App;
