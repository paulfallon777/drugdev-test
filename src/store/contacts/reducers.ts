import {
  CREATE_CONTACT,
  DELETE_CONTACT,
  GET_CONTACT,
  CLEAR_CONTACT,
  EDIT_CONTACT,
  ContactState,
  ContactActionTypes,
  Person,
  ContactId,
  Contact
  } from "./types";
  
  const initial: ContactState = {
    list: []
  };
  
  export function contacts(
    state = initial,
    action: ContactActionTypes
  ): ContactState {
    switch (action.type) {
      case CREATE_CONTACT:
        return {
          list: [
            { ...(action.payload as Person), id: state.list.length },
            ...state.list
          ]
        };
      case GET_CONTACT:
        return {
          viewing: state.list.find(c => c.id === (action.payload as ContactId).id),
          ...state
        };
      case CLEAR_CONTACT:
        return {
          viewing: undefined,
          ...state
        };
      case EDIT_CONTACT:
        return {
          list: state.list.map(contact => {
            if (contact.id === (action.payload as Contact).id) {
              return action.payload as Contact;
            }
            return contact;
          })
        };
      case DELETE_CONTACT:
        return {
          list: state.list.filter(
            contact => contact.id !== (action.payload as ContactId).id
          )
        };
      default:
        return state;
    }
  }
  