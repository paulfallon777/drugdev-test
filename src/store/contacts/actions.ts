import {
    CREATE_CONTACT,
    EDIT_CONTACT,
    DELETE_CONTACT,
    GET_CONTACT,
    CLEAR_CONTACT,
    Contact
  } from "./types";
  
  export const createContact = (name: string, email: string) => ({
    type: CREATE_CONTACT,
    payload: { name, email }
  });
  
  export const editContact = (contact: Contact) => ({
    type: EDIT_CONTACT,
    payload: contact
  });
  
  export const deleteContact = (id: number) => ({
    type: DELETE_CONTACT,
    payload: { id }
  });
  
  export const getContact = (id: number) => ({
    type: GET_CONTACT,
    payload: { id }
  });
  
  export const clearContact = () => ({
    type: CLEAR_CONTACT
  });
  