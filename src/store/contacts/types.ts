export const CREATE_CONTACT: string = "CREATE_CONTACT";
export const DELETE_CONTACT: string = "DELETE_CONTACT";
export const EDIT_CONTACT: string = "EDIT_CONTACT";
export const GET_CONTACT: string = "GET_CONTACT";
export const CLEAR_CONTACT: string = "CLEAR_CONTACT";

export interface Person {
  name: string;
  email: string;
}

export interface ContactId {
  id: number;
}
export interface Contact extends Person, ContactId {}

export interface ContactState {
  list: Contact[];
  viewing?: Contact | null;
}

interface CreateContactAction {
  type: typeof CREATE_CONTACT;
  payload: Person;
}

interface DeleteContactAction {
  type: typeof DELETE_CONTACT;
  payload: ContactId;
}

interface GetContactAction {
  type: typeof GET_CONTACT;
  payload: ContactId;
}

interface ClearContactAction {
  type: typeof CLEAR_CONTACT;
  payload?: void;
}

interface EditContactAction {
  type: typeof EDIT_CONTACT;
  payload: Contact;
}

export type ContactActionTypes =
  | CreateContactAction
  | DeleteContactAction
  | EditContactAction
  | GetContactAction
  | ClearContactAction;
