import React from "react";
import Form from "../Form/Form";

const CreateContact = () => (
  <div>
    <h2>Create New Contact</h2>
    <Form />
  </div>
);

export default CreateContact;
