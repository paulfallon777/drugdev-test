import React, { SyntheticEvent, useState, useEffect } from "react";
import { Redirect } from "react-router-dom";
import { connect } from "react-redux";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import { Contact } from "../../store/contacts/types";
import { createContact, editContact } from "../../store/contacts/actions";

interface Props {
  contact?: Contact;
  createContact(name: string, email: string): void;
  editContact(contact: Contact): void;
}

export const Form = ({ createContact, editContact, contact }: Props) => {
  const [contactData, setContact] = useState({
    id: -1,
    name: "",
    email: ""
  });
  const [finished, setFinished] = useState(false);

  useEffect(() => {
    if (!!contact) {
      setContact(contact);
    }
  }, []);

  const handleChange = (name: string) => (event: SyntheticEvent) => {
    setContact({
      ...contactData,
      [name]: (event.target as HTMLInputElement).value
    });
  };

  const onSubmit = (event: SyntheticEvent) => {
    event.preventDefault();
    if (!contact) {
      createContact(contactData.name, contactData.email);
    } else {
      editContact(contactData);
    }
    setFinished(true);
  };
  return (
    <form onSubmit={onSubmit}>
      <TextField
        id="name"
        label="Name"
        value={contactData.name}
        onChange={handleChange("name")}
        margin="normal"
      />
      <TextField
        id="email"
        label="Email Address"
        value={contactData.email}
        onChange={handleChange("email")}
        margin="normal"
      />
      <div>
        <Button type="submit" variant="outlined" color="primary">
          {!contact ? "Create" : "Update"}
        </Button>
      </div>
      {finished && <Redirect to="/contacts" />}
    </form>
  );
};

const mapStateToProps = null;

const mapDispatchToProps = (dispatch: any) => ({
  createContact: (name: string, email: string) =>
    dispatch(createContact(name, email)),
  editContact: (contact: Contact) => dispatch(editContact(contact))
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Form);
