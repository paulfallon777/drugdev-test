import React, { useState } from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { Contact } from "../../store/contacts/types";
import { deleteContact } from "../../store/contacts/actions";
import Card from "@material-ui/core/Card";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";

interface Props {
  contact: Contact;
  deleteContact(id: number): void;
}

export const View = ({ contact: {id, name, email}, deleteContact }: Props) => {
  const onDelete = (): void => {
    deleteContact(id);
  };

  const MyLink = (props: any) => {
    return <Link to={`/contact/${id}/edit`} {...props} />;
  };

  return (
    <Card>
      <CardContent>
        <Typography color="textSecondary" gutterBottom>
          {name}
        </Typography>
        <Typography variant="h5" component="h2">
          {email}
        </Typography>
        <Typography color="textSecondary">ID: {id}</Typography>
      </CardContent>
      <CardActions>
        <Button color="primary" size="medium" component={MyLink}>
          Edit Contact
        </Button>
        <Button color="primary" size="medium" onClick={onDelete}>
          Delete Contact
        </Button>
      </CardActions>
    </Card>
  );
};

const mapStateToProps = null;

const mapDispatchToProps = (dispatch: any) => ({
  deleteContact: (id: number) => dispatch(deleteContact(id))
});
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(View);
