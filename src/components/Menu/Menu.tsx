import React from "react";
import { Link } from "react-router-dom";

const Menu = () => (
  <nav className="Menu">
    <Link className="link" to="/contacts">
      View Current Contacts
    </Link>
    {`   ***   `}
    <Link className="link" to="/contacts/new">
      Add a New Contact
    </Link>
  </nav>
);

export default Menu;
