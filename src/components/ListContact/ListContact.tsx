import React from "react";
import List from "../List/List";

const ListContact = () => (
  <div>
    <h2>List of Contacts</h2>
    <List />
  </div>
);

export default ListContact;
