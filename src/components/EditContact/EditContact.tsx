import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import { RouteComponentProps } from "react-router-dom";
import { Contact } from "../../store/contacts/types";
import { State } from "../../store/types";
import Form from "../Form/Form";
import { getContact, clearContact } from "../../store/contacts/actions";

export type TParams = { id: string };

interface Props extends RouteComponentProps<TParams> {
  contact?: Contact | null;
  getContact(id: string): void;
  clearContact(): void;
}

const EditContact = ({ match, contact, getContact, clearContact }: Props) => {
  const [found, setFound] = useState(false);

  useEffect(() => {
    getContact(match.params.id);

    return () => {
      clearContact();
    };
  }, []);

  useEffect(() => {
    if (!contact && !found) {
      setFound(true);
    }
  }, [contact]);

  if (!!contact && !found) {
    setFound(true);
  }

  return (
    <div>
      <h2>Edit Contact</h2>
      <div className="Edit-container">
        {contact ? <Form contact={contact} /> : <div>Loading....</div>}
        {!contact && found && <Redirect to="/contacts" />}
      </div>
    </div>
  );
};

const mapStateToProps = (state: State) => ({
  contact: state.contacts.viewing
});

const mapDispatchToProps = (dispatch: any) => ({
  getContact: (id: string) => {
    dispatch(getContact(parseInt(id, 10)));
  },
  clearContact: () => dispatch(clearContact())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(EditContact);
