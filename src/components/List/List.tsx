import React from "react";
import { connect } from "react-redux";
import { Link } from "react-router-dom";
import { State } from "../../store/types";
import { ContactState } from "../../store/contacts/types";

export interface Props {
  contacts: ContactState;
}

export const List = ({ contacts }: Props) => {
  return (
    <ul className="List">
      {contacts && contacts.list.length ? (
        contacts.list.map(({id, name}) => {
          return (
          <li key={id}>
            <Link to={`/contact/${id}`}>{name}</Link>
          </li>
        )})
      ) : (
        <p>Empty Contact List</p>
      )}
    </ul>
  );
};

const mapStateToProps = (state: State) => ({
  contacts: state.contacts
});

export default connect(mapStateToProps)(List);
