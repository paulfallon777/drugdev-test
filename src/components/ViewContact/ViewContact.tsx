import React, { useEffect, useState } from "react";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import View from "../View/View";
import { RouteComponentProps } from "react-router-dom";
import { Contact } from "../../store/contacts/types";
import { State } from "../../store/types";
import { getContact, clearContact } from "../../store/contacts/actions";

export type TParams = { id: string };

interface Props extends RouteComponentProps<TParams> {
  contact?: Contact | null;
  getContact(id: string): void;
  clearContact(): void;
}

const ViewContact = ({ match, contact, getContact, clearContact }: Props) => {
  const [valid, setFound] = useState(false);

  useEffect(() => {
    getContact(match.params.id);

    return () => {
      clearContact();
    };
  }, []);

  useEffect(() => {
    if (contact && !valid) {
      setFound(true);
    }
  }, [contact]);

  return (
    <div>
      <h2>View Contact</h2>
      <div className="View-container">
        {!!contact ? <View contact={contact} /> : <div>Loading...</div>}
      </div>
      {!contact && valid && <Redirect to="/contacts" />}
    </div>
  );
};

const mapStateToProps = (state: State) => ({
  contact: state.contacts.viewing
});

const mapDispatchToProps = (dispatch: any) => ({
  getContact: (id: string) => {
    dispatch(getContact(parseInt(id, 10)));
  },
  clearContact: () => dispatch(clearContact())
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ViewContact);
